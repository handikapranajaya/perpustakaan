from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='perpustakaan',
                              user='root',
                              password='GriyaRosa3657')
        except Exception as e:
            print(e)
    
    def showUsers(self):
        try:  
            cursor = self.db.cursor()
            query ='''select * from customers'''
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            print(e)
    
    def showUserById(self, param):
        try:
            cursor = self.db.cursor()
            query = '''
                        SELECT * 
                        FROM customers 
                        WHERE USERID = {0};
                    '''.format(param)
            
            cursor.execute(query)
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
    
    def insertUser(self, **params):
        try:
            crud_query = '''
                            INSERT INTO customers (USERNAME, NAMADEPAN, NAMABELAKANG, EMAIL) 
                            VALUES('{0}', '{1}', '{2}', '{3}')
                        '''.format(params['USERNAME'], params['NAMADEPAN'], params['NAMABELAKANG'], params['EMAIL'])

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return 'Data berhasil disimpan'            
        except Exception as e:
            print(e)
    
    def updateUserById(self, **params):
        try:
            crud_query = '''
                            UPDATE customers
                            SET USERNAME = '{1}', NAMADEPAN = '{2}', NAMABELAKANG = '{3}', EMAIL = '{4}'
                            WHERE USERID = {0}
                        '''.format(params['USERID'], params['USERNAME'], params['NAMADEPAN'], params['NAMABELAKANG'], params['EMAIL'])

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return 'Data berhasil diubah'
        except Exception as e:
            print(e)
    
    def deleteUserById(self, **params):
        try:
            crud_query = '''
                            DELETE FROM customers 
                            WHERE USERID = {0}
                        '''.format(params['USERID'])

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return 'Data berhasil dihapus'
        except Exception as e:
            print(e)    
    
