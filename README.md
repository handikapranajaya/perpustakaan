**Panduan penggunaan aplikasi.**

Aplikasi ini dibangun menggunakan framework Flask dengan menggunakan Mysql-Connector untuk akses ke database nya, Silahkan akses aplikasi tsb pada direktori mysqlapp.

Untuk melihat struktur table yang digunakan silahkan lihat pada gambar terlampir (ER-Diagram.png).

Silahkan akses endpoints menggunakan Postman, adapun list endpoints sebagai berikut:

- Untuk menampilkan seluruh data pada table customer (METHOD="GET")
    http://127.0.0.1:5000/users

- Untuk menampilkan spesifik 1 user berdasarkan id dari table customer (METHOD="GET")
    http://127.0.0.1:5000/user/3

- Untuk menambahkan data pada table customer (METHOD="POST")
    http://127.0.0.1:5000/user

- Untuk melakukan perubahan pada table customer (METHOD="PUT")
    http://127.0.0.1:5000/user

- Untuk melakukan hapus data pada table customer (METHOD="DELETE")
    http://127.0.0.1:5000/user

